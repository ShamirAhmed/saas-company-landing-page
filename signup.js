function validateForm(event) {
    event.preventDefault();

    const firstName = document.querySelector('#first-name');
    const lastName = document.querySelector('#last-name');
    const email = document.querySelector('#email');
    const password = document.querySelector('#password');
    const repeatPassword = document.querySelector('#repeat-password');
    const tos = document.querySelector('#tos');
    // const disableRed = document.querySelectorAll("input");
    const msg1 = document.querySelector('#msg1');
    const msg2 = document.querySelector('#msg2');
    const msg3 = document.querySelector('#msg3');
    const msg4 = document.querySelector('#msg4');
    const msg5 = document.querySelector('#msg5');
    const msg6 = document.querySelector('#msg6');
    const msg = document.querySelectorAll('.msg-color');
    const successMsg = document.querySelector('#success');

    let success = true;

    firstName.value = firstName.value.trim();
    lastName.value = lastName.value.trim();
    email.value = email.value.trim();

    if (firstName.value === ""){
        firstName.style.border = "1px solid red";
        msg1.classList.remove('hidden');
        success = false;
    } else if (firstName.value.split(' ').length > 1){
        firstName.style.border = "1px solid red";
        msg1.classList.remove('hidden');
        success = false;
    } else if (firstName.value.match(/[^\w ]/gi) !== null) {
        firstName.style.border = "1px solid red";
        msg1.classList.remove('hidden');
        success = false;
    } else if (firstName.value.match(/[\d]/gi) !== null) {
        firstName.style.border = "1px solid red";
        msg1.classList.remove('hidden');
        success = false;
    } else if (firstName.value.length < 2) {
        firstName.style.border = "1px solid red";
        msg1.classList.remove('hidden');
        success = false;
    } else {
        firstName.style.border = "1px solid green";
        msg1.classList.add('hidden');
    }

    if (lastName.value === ""){
        lastName.style.border = "1px solid red";
        msg2.classList.remove('hidden');
        success = false;
    } else if (lastName.value.split(' ').length > 1){
        lastName.style.border = "1px solid red";
        msg2.classList.remove('hidden');
        success = false;
    } else if (lastName.value.match(/[^\w ]/gi) !== null) {
        lastName.style.border = "1px solid red";
        msg2.classList.remove('hidden');
        success = false;
    } else if (lastName.value.match(/[\d]/gi) !== null) {
        lastName.style.border = "1px solid red";
        msg2.classList.remove('hidden');
        success = false;
    } else if (lastName.value.length < 2) {
        lastName.style.border = "1px solid red";
        msg2.classList.remove('hidden');
        success = false;
    } else if (firstName.value === lastName.value) {
        lastName.style.border = "1px solid red";
        msg2.classList.remove('hidden');
        success = false;
    } else {
        lastName.style.border = "1px solid green";
        msg2.classList.add('hidden');
    }

    const reg = /^(([^<>()[\]\\.,:\s@"]+(\.[^<>()[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/gi;

    if (email.value === "") {
        email.style.border = "1px solid red";
        msg3.classList.remove('hidden');
        success = false;
    } else if (reg.test(email.value) === false) {
        email.style.border = "1px solid red";
        msg3.classList.remove('hidden');
        success = false;
    } else {
        email.style.border = "1px solid green";
        msg3.classList.add('hidden');
    }

    if (password.value.length < 6 || password.value === '') {
        password.style.border = "1px solid red";
        msg4.classList.remove('hidden');
        success = false;
    } else {
        password.style.border = "1px solid green";
        msg4.classList.add('hidden');
    }

    if (repeatPassword.value !== password.value || repeatPassword.value.length < 6) {
        repeatPassword.style.border = "1px solid red";
        msg5.classList.remove('hidden');
        success = false;
    } else {
        repeatPassword.style.border = "1px solid green";
        msg5.classList.add('hidden');
    }

    if (tos.checked !== true) {
        msg6.classList.remove('hidden');
        success = false;
    } else {
        msg6.classList.add('hidden');
    }

    if(success === true) {
        successMsg.classList.remove('hidden');
        firstName.value = "";
        lastName.value = "";
        email.value = "";
        password.value = "";
        repeatPassword.value = "";
        tos.checked = false;
    }

    // console.log(firstName.value.match(/[^a-z]/gi));

    /* for (let index = 0; index < disableRed.length; index++) {
        disableRed[index].style.removeProperty('border');
        msg[index].classList.add('hidden');
    }

    if (firstName.value === "") {
        firstName.style.border = "1px solid red";
        msg1.classList.remove('hidden');
        return false;
    } else if (lastName.value === "") {
        lastName.style.border = "1px solid red";
        msg2.classList.remove('hidden');
        return false;
    } else if (email.value === "") {
        email.style.border = "1px solid red";
        msg3.classList.remove('hidden');
        return false;
    } else if (password.value === "") {
        password.style.border = "1px solid red";
        msg4.classList.remove('hidden');
        return false;
    } else if (password.value !== repeatPassword.value) {
        repeatPassword.style.border = "1px solid red";
        msg5.classList.remove('hidden');
        return false;
    } else if (tos.checked === false) {
        msg6.classList.remove('hidden');
        return false;
    } else {
        success.classList.remove('hidden');
        return false;
    } */
}